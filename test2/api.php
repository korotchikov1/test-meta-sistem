<?php
$targetDir = 'uploads/';

if (!file_exists($targetDir)) {
    mkdir($targetDir, 0777, true);
}

$chunkNumber = intval($_POST['chunkNumber']);
$totalChunks = intval($_POST['totalChunks']);
$file = $_FILES['file'];

$originalFileName = $_POST['fileName'];
$fileExtension = $_POST['fileExtension'];

$fileName = $targetDir . basename($originalFileName . '.part' . $chunkNumber . '.' . $fileExtension);
move_uploaded_file($file['tmp_name'], $fileName);

if ($chunkNumber == $totalChunks) {
    $outputFileName = $targetDir . basename($originalFileName . '.' . $fileExtension);
    $outputFile = fopen($outputFileName, 'wb');
    for ($i = 1; $i <= $totalChunks; $i++) {
        $partFileName = $targetDir . basename($originalFileName . '.part' . $i . '.' . $fileExtension);
        $partFile = fopen($partFileName, 'rb');
        while ($data = fread($partFile, 4096)) {
            fwrite($outputFile, $data);
        }
        fclose($partFile);
        unlink($partFileName);
    }
    fclose($outputFile);
    echo 'Файл успешно загружен.';
} else {
    echo 'Часть файла загружена.';
}