<?php

class Arithmetics
{

    private $expression;

    public function __construct($str)
    {
        $this->expression = $str;
    }

    public function setExpression($str)
    {
        $this->expression = $str;
    }

    public function getExpression()
    {
        return $this->expression;
    }

    public function checkExpression()
    {
        $stack = [];
        $brackets = ['(' => ')', '[' => ']', '{' => '}'];

        $expression = $this->getExpression();

        foreach (str_split($expression) as $char) {
            if (in_array($char, array_keys($brackets))) {
                array_push($stack, $char);
            } elseif (in_array($char, array_values($brackets))) {
                $lastBracket = array_pop($stack);
                if (!$lastBracket || $brackets[$lastBracket] !== $char) {
                    return false;
                }
            }
        }

        if (!empty($stack))
            return false;
        return true;
    }
}

$test1 = new Arithmetics('[({})]');
echo ($test1->checkExpression()) ? 'Вірно' : 'Невірно';

$test2 = new Arithmetics('[([)');
echo ($test2->checkExpression()) ? 'Вірно' : 'Невірно';
