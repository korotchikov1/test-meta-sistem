SELECT id, COUNT(id) as count
FROM table
GROUP BY id
HAVING count > 1;
